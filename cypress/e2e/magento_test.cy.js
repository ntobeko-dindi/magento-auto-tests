/// <reference types="cypress" />


describe('magento tests', () => {
   beforeEach(() => {
      cy.visit('https://magento.softwaretestingboard.com/')
   })

   it('should display search results containing the keyword "hoodie"', () => {
      let searchQuery = 'hoodie'
      cy.get('#search').type(searchQuery)
      cy.get('.action.search').click()
      cy.get('.product-item-link').each(($el) => {
         expect($el.text().toLowerCase()).to.contain(searchQuery)
      })
   })

   it('should display an error message when trying to add an item to wishlist without signing in', () => {
      cy.get('.product-item-link').first().click()
      cy.get('.action.tocart.primary').first().click()
      cy.get('#option-label-size-143-item-166').click()
      cy.get('#option-label-color-93-item-50').click()
      cy.get('.towishlist').click()
      cy.get('.message-error').should('be.visible')
      cy.get('.error').should('contain', 'You must login or register to add items to your wishlist.')
   })

   it('should add an item to the compare list', () => {
      cy.get('#option-label-size-143-item-166').click()
      cy.get('.tocompare').first().click({ force: true })
      cy.get('.message-success', { timeout: 10000 }).should('be.visible')
   })

   it('should clear all items from the compare list', () => {
      tearDown()
   })
})

function tearDown() {
   cy.get('#option-label-size-143-item-166').click()
   cy.get('.tocompare').first().click({ force: true })
   cy.get('#ui-id-3').click()
   cy.get('#compare-clear-all').click()
   cy.get('.action-accept').click()
   cy.get('.empty', { timeout: 10000 }).should('be.visible')
   cy.get('.message-success', { timeout: 10000 }).should('contain', 'You cleared the comparison list.')
}
